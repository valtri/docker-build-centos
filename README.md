# Info

Simple CentOS images for build.

# Tags

CentOS 8 (openjdk 8):

* *8*: packaging
* *8-jni*: java + maven + gcc
* *8-maven*: java
